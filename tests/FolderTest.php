<?php
declare(strict_types=1);


use Insidesuki\Utilities\File\File;
use Insidesuki\Utilities\File\Folder;
use PHPUnit\Framework\TestCase;

class FolderTest extends TestCase
{

    public function testFailPathDoesNotExists():void{

        $path = __DIR__.'/../var/none';
        $this->expectExceptionMessage(sprintf('Folder:%s not exists!!!',$path));
        Folder::fromPath($path,false);

    }

    public function testOkGetFilesFolder(){

        $path = '/tmp';
        $tmp = Folder::fromPath($path,false);
        $this->assertNotEmpty($tmp->files());
        $this->assertTrue($tmp->writable);

    }

	public function testOkGetRealFilesFolder(){

		$path = __DIR__ . '/../src/Examples/';
		$tmp = Folder::fromPath($path,false);
		$files = $tmp->realFiles();
		$firstFile = $files[0];
		$this->assertInstanceOf(File::class,$firstFile);


	}

    public function testOkFolderNotWriteable(){

        $path = '/etc';
        $tmp = Folder::fromPath($path,false);
        $this->assertFalse($tmp->writable);

    }

    public function testFailPathDoesNotExistsAndReadOnly():void{

        $this->expectExceptionMessage(sprintf('Cant create folder %s, check permissions!!!','/etc/newfolder'));
        $path = '/etc/newfolder';
        Folder::fromPath($path,true);

    }

    public function testFailFolderCantRemove():void{

        $this->expectExceptionMessage(sprintf('Cant remove folder %s, is not empty!!!','/tmp'));
        $path = '/tmp';
        $tmp = Folder::fromPath($path,false);
        $tmp->remove();

    }

    public function testOkPathDoesNotExistsAndWasCreated():void{

        $path = __DIR__.'/../var/none';
        $none = Folder::fromPath($path,true);
        $this->assertSame(realpath($path),$none->fullpath);
        $none->remove();;
        $this->assertFalse(realpath($path));

    }

    public function testOkCompressWithSameName():void{

        $path = realpath(__DIR__ . '/../src/Examples/');
        $pathZip = __DIR__.'/../var/';
        $folder = Folder::fromPath($path);
        $zip = $folder->compress(Folder::fromPath($pathZip));
        $this->assertFileExists($pathZip.'/Examples.zip');

    }

    public function testOkCompressWithDifferentName():void{

        $path = realpath(__DIR__ . '/../src/Examples/');
        $pathZip = __DIR__.'/../var/';
        $folder = Folder::fromPath($path);
        $zip = $folder->compress(Folder::fromPath($pathZip),'test.zip');
        $this->assertFileExists($pathZip.'/test.zip');

    }


}
