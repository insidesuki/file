<?php
declare(strict_types=1);

namespace Insidesuki\Utilities\File\Exceptions;

use RuntimeException;

class FileException extends RuntimeException
{

    private function __construct(string $message)
    {
        parent::__construct($message);
    }

    public static function copy(string $file, string $output):self
    {
        throw new self(sprintf('When try to copy %s on %s',$file, $output));
    }
}