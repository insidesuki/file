<?php
declare(strict_types=1);

namespace Insidesuki\Utilities\File;

use Insidesuki\Utilities\File\Exceptions\FileDoesNotExistsException;
use Insidesuki\Utilities\File\Exceptions\FileException;
use Insidesuki\Utilities\File\Exceptions\FolderDoesNotExistsException;
use RuntimeException;

/**
 * File
 * @author diemarc@protonmail.com
 */
class File
{
    public readonly string $fileName;
    public readonly string $baseName;
    public readonly string $extension;
    public readonly Folder $folder;
    public readonly string $fullPath;

    /**
     * @throws FileDoesNotExistsException
     */
    private function __construct(
        string                 $fullPath,
        public readonly bool   $validStamp = false,
        public readonly string $stampInfo = ''
    )
    {

        $this->fullPath = realpath($fullPath);
        $data = pathinfo($this->fullPath);
        $this->folder = Folder::fromPath($data['dirname']);
        $this->fileName = $data['filename'];
        $this->extension = $data['extension'];
        $this->baseName = $data['basename'];

    }

    /**
     * @throws FileDoesNotExistsException
     */
    public static function fromPath(string $fullPath): static
    {
        self::checkFile($fullPath);
        return new static($fullPath);

    }

    /**
     * @throws FileDoesNotExistsException
     */
    private static function checkFile(string $fullPath): void
    {
        if (false === is_file($fullPath)) {
            throw new FileDoesNotExistsException($fullPath);
        }


    }

    /**
     * @param string $base64
     * @param string $outputFolder
     * @param string $outputFile
     * @return static
     * @throws FileDoesNotExistsException
     */
    public static function fromBase64(string $base64, string $outputFolder, string $outputFile): static
    {

        $folder = Folder::fromPath($outputFolder,true);
        $binaryDoc = base64_decode($base64);
        $extension = explode('/', finfo_buffer(finfo_open(), $binaryDoc, FILEINFO_MIME_TYPE))[1];

        $fullPathFile = $folder->fullpath . '/' . $outputFile . '.' . $extension;;
        if (false === @file_put_contents($fullPathFile, $binaryDoc)) {

            throw new RuntimeException('Error at create file from base64');
        }

        return new static($fullPathFile);

    }

    /**
     * @throws FileDoesNotExistsException
     */
    public static function fromStamped(string $fullPath, bool $valid, string $stampInfo): static
    {

        self::checkFile($fullPath);
        return new static($fullPath, $valid, $stampInfo);
    }

    public function toBase64(): string
    {

        $handle = @fopen($this->fullPath, 'rb');


        if ($handle === false) {
            throw new RuntimeException('Error at fopen the file');
        }

        $contents = fread($handle, filesize($this->fullPath));
        fclose($handle);

        return base64_encode($contents);


    }


    public function toBase64Png(): string
    {

        return 'data:image/png;base64,'.$this->toBase64();

    }

    public function copy(string $destination, string $newFileName = ''): static
    {

        if (!is_dir($destination)) {
            throw new FolderDoesNotExistsException($destination);
        }

        $fileName = (empty($newFileName)) ? $this->baseName : $newFileName;

        $outputFullPath = $destination . '/' . $fileName;


        if (false === @copy($this->fullPath, $outputFullPath)) {
            FileException::copy($fileName, $outputFullPath);
        }

        return static::fromPath($outputFullPath);


    }

    public function content(): string
    {

        return file_get_contents($this->fullPath);

    }

    public function isPdf(): bool
    {
        return 'application/pdf' === $this->mime();
    }

    public function mime(): string
    {
        return mime_content_type($this->fullPath);
    }

}