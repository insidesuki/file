<?php
declare(strict_types=1);

namespace Insidesuki\Utilities\File;

use Insidesuki\Utilities\File\Exceptions\FileDoesNotExistsException;
use RuntimeException;
use ZipArchive;

/**
 * Folder
 * @author diemarc@protonmail.com
 */
class Folder
{
    private function __construct(
        public string $fullpath,
        public readonly bool   $writable
    ){}

    public static function fromPath(string $path, bool $create = false): static
    {

        if (!is_dir($path)) {

            if (false === $create) {
                throw new RuntimeException(sprintf('Folder:%s not exists!!!', $path));
            }

            if (@!mkdir($path, 0777, true) && !is_dir($path)) {
                throw new RuntimeException(sprintf('Cant create folder %s, check permissions!!!', $path));
            }
        }

        return new static(realpath($path), is_writable($path));


    }

    public function remove(): void
    {

        if (!$this->empty()) {
            throw new RuntimeException(sprintf('Cant remove folder %s, is not empty!!!',$this->fullpath));
        }

        rmdir($this->fullpath);

    }

    public function files(): array
    {

        return scandir($this->fullpath);

    }
	/**
	 * @throws FileDoesNotExistsException
	 */
	public function realFiles(): array
	{

		$files = [];

		$filesDir = $this->files();

		foreach ($filesDir as $file) {

			if (false === is_dir($file)) {
				if ($file !== '.' && $file !== '..') {

					$files[] = File::fromPath($this->fullpath.'/'.$file);
				}
			}
		}

		return $files;


	}



    public function empty(): bool
    {
        return (count(scandir($this->fullpath)) === 2);
    }

    public function compress(Folder $outputPath, ?string $zipFileName = null): ZipArchive
    {

        $zip = new ZipArchive();

        $pathInfo = pathinfo($this->fullpath);
        $zipName = $zipFileName ?? ($pathInfo['filename'] . '.zip');
        $zipFile = $outputPath->fullpath.'/'.$zipName;

        if ($zip->open($zipFile, ZipArchive::CREATE) !== TRUE) {
            throw new RuntimeException(sprintf('Error on open :%s', $zipName));
        }

        $files = $this->files();

        foreach ($files as $file) {

            $exluded = ['.', '..'];

            if (!in_array($file, $exluded, true)) {

                $filename = $this->fullpath . '/' . $file;
                $zip->addFile($filename, $file);
            }

        }
        $zip->close();

        return $zip;

    }


}